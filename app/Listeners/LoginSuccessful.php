<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Session;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\Activitylog\Models\Activity;

class LoginSuccessful
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $event->subject = 'Teacher Logged in Successful';
        $event->description = 'Login Succesfully';

        Session::flash('login-success', 'Hello' . $event->user->first_name . ', Welcome Back!');
        if($event->user->isTeacher()) {
            activity($event->subject)
                ->by($event->user)
                ->withProperties([
                    'id' => $event->user->id,
                    'email' => $event->user->email,
                    'first_name' => $event->user->first_name,
                    'last_name' => $event->user->last_name,
                ])
                ->log($event->description);
        }
    }
}
