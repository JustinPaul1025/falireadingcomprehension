<?php

namespace App\Types;

final class RoleType
{
    const ADMINISTRATOR = 'Administrator';
    const TEACHER = 'Teacher';
    const STUDENT = 'Student';

    public static function toArray(): array
    {
        return [
            self::ADMINISTRATOR,
            self::TEACHER,
            self::STUDENT
        ];
    }
}