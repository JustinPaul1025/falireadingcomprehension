<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;

class Quiz extends Model implements HasMedia
{
    use InteractsWithMedia;
    
    protected $guarded = [];

    protected $casts = [
        'values' => 'array',
    ];

    public function readingMaterial()
    {
        return $this->belongsTo(ReadingMaterial::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('quiz_img')->singleFile();
    }
}
