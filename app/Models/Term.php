<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;

class Term extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $guarded = [];

    public function readingMaterial()
    {
        return $this->belongsTo(ReadingMaterial::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('term_img')->singleFile();
    }
}
