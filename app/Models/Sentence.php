<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;

class Sentence extends Model implements HasMedia
{
    use InteractsWithMedia;
    
    protected $guarded = [];

    public function readingMaterial()
    {
        return $this->belongsTo(ReadingMaterial::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('sentence_img')->singleFile();
    }
}
