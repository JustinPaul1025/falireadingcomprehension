<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function readingMaterials()
    {
        return $this->hasMany(ReadingMaterial::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
