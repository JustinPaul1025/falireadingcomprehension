<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Simulation extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = [
        'title',
        'uploader',
        'file_type'
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('videos')->singleFile();
        $this->addMediaCollection('thumbnail')->singleFile();
    }
}
