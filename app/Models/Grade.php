<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $guarded = [];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
    
    public function readingMaterial()
    {
        return $this->belongsTo(ReadingMaterial::class);
    }

    public function miscue()
    {
        return $this->hasOne(Miscue::class);
    }
}
