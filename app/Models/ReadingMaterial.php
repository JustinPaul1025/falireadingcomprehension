<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;

class ReadingMaterial extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $guarded = [];

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function quizzes()
    {
        return $this->hasMany(Quiz::class);
    }

    public function terms()
    {
        return $this->hasMany(Term::class);
    }

    public function grades()
    {
        return $this->hasMany(Grade::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function sentences()
    {
        return $this->hasMany(Sentence::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('story_img')->singleFile();
    }
}
