<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Miscue extends Model
{
    protected $guarded = [];

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }
}
