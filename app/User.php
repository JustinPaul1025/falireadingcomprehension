<?php

namespace App;

use App\Models\Section;
use App\Types\RoleType;
use Spatie\MediaLibrary\HasMedia;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, HasRoles, LogsActivity, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $table = "users";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static $logAttributes = [
        'first_name',
        'last_name',
        'email'
    ];

    protected static $ignoreChangedAttributes = [
        'password',
        'updated_at'
    ];

    protected static $recordEvents = [
        'created',
        'updated'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getDescriptionForEvent(string $eventName): string 
    {
        return "you have {$eventName} user";
    }

    public function isAdmin(): bool
    {
        return $this->hasRole(RoleType::ADMINISTRATOR);
    }

    public function isTeacher(): bool
    {
        return $this->hasRole(RoleType::TEACHER);
    }

    public function isStudent(): bool
    {
        return $this->hasRole(RoleType::STUDENT);
    }

    public function sections()
    {
        return $this->hasMany(Section::class);
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    public function routeNotificationForNexmo($notification)
    {
        $number = '+63';
        if($this->mother_contact_number)
        return $this->phone_number;
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('audio')->singleFile();
    }

    public function audioUrl()
    {
        $profile = collect($this->media)->firstWhere('collection_name', 'audio');

        return optional($profile)->getUrl();
    }
}
