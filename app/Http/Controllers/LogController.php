<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class LogController extends Controller
{
    public function list()
    {
        return response()->json(Activity::orderBy('created_at', 'desc')->get());
    }
}
