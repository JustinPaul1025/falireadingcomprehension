<?php

namespace App\Http\Controllers;

use App\Models\Term;
use Illuminate\Http\Request;
use App\Models\ReadingMaterial;
use App\Http\Resources\Term\TermCollection;

class TermController extends Controller
{
    public function index(ReadingMaterial $readingMaterial)
    {
        $term = $readingMaterial->terms;

        return new TermCollection($term);
    }

    public function store(Request $request)
    {
        $request->validate([
            'word' => 'required|unique:terms|max:255',
        ]);

        $term = Term::create([
            'reading_material_id' => $request->input('id'),
            'word' => $request->input('word'),
            'definition' => $request->input('definition')
        ]);

        if($request->hasFile('term_img')) {
            $term->addMedia($request->file('term_img'))->toMediaCollection('term_img');
        }

        return response()->json('done');
    }
    
    public function update(Request $request)
    {
        $term = Term::findOrFail($request->input('id'));

        $request->validate([
            'word' => 'required|unique:terms,word,' . $term->id,
        ]);

        $term->update([
            'word' => $request->input('word'),
            'definition' => $request->input('definition')
        ]);

        if($request->hasFile('term_img')) {
            $media = collect($term->media)->firstWhere('collection_name', 'term_img');
            if($media != null) {
                $media->delete();
            }

            $term->addMedia($request->file('term_img'))->toMediaCollection('term_img');
        }

        return response()->json('done');
    }

    public function destroy(Term $term)
    {
        $media = collect($term->media)->firstWhere('collection_name', 'term_img');
        if($media != null) {
            $media->delete();
        }

        return response()->json($term->delete());
    }
}
