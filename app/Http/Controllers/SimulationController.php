<?php

namespace App\Http\Controllers;

use App\Models\Simulation;
use Illuminate\Http\Request;
use App\Http\Resources\Simulation\SimulationCollection;

class SimulationController extends Controller
{
    public function list(Request $request)
    {
        $simulations = Simulation::query();

        if($request->filled('search')) {
            $simulations = $simulations->where('title', 'LIKE', '%'.$request->input('search').'%');
        }
        
        $simulations = $simulations->paginate(10);

        return new SimulationCollection($simulations);
    }

    public function store(Request $request)
    {
        if(!$request->hasFile('video')) {
            return response()->json('please add video');
        }

        $request->validate([
            'title' => 'required|unique:simulations|max:255',
        ]);

        beginTransaction();
        try {
            $simulation = Simulation::create([
                'title' => $request->input('title'),
                'file_type' => 'video/'.$request->video->getClientOriginalExtension(),
                'uploader' => auth()->user()->first_name.' '.auth()->user()->last_name
            ]);

            $simulation->addMedia($request->file('video'))->toMediaCollection('video');

            if($request->hasFile('thumbnail')) {
                $simulation->addMedia($request->file('thumbnail'))->toMediaCollection('thumbnail');
            }

            commit();
        } catch (Exception $e) {
            rollback();
            throw $e;
        }

        return response()->json($simulation);
    }

    public function update(Request $request)
    {
        $simulation = Simulation::findOrFail($request->input('id'));

        $request->validate([
            'title' => 'required|max:255|unique:simulations,title,' . $simulation->id
        ]);

        beginTransaction();
        try {
            $simulation->update([
                'title' => $request->input('title')
            ]);

            if($request->file('video')) {
                $media = collect($simulation->media)->firstWhere('collection_name', 'video');
                
                if($media != null) {
                    $media->delete();
                }
                
                $simulation->addMedia($request->file('video'))->toMediaCollection('video');

                $simulation->update([
                    'file_type' => 'video/'.$request->video->getClientOriginalExtension(),
                    'uploader' => auth()->user()->first_name.' '.auth()->user()->last_name
                ]);
            }

            if($request->hasFile('thumbnail')) {
                $media = collect($simulation->media)->firstWhere('collection_name', 'thumbnail');

                if($media != null) {
                    $media->delete();
                }

                $simulation->addMedia($request->file('thumbnail'))->toMediaCollection('thumbnail');
            }

            commit();
        } catch (Exception $e) {
            rollback();
            throw $e;
        }

        return response()->json($simulation);
    }

    public function destroy(Simulation $simulation)
    {
        $media = collect($simulation->media)->firstWhere('collection_name', 'video');
        if($media != null) {
            $media->delete();
        }

        return response()->json($simulation->delete());
    }
}
