<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Resources\Grade\GradeCollection;

class GradeController extends Controller
{
    public function studentGrade(Student $student)
    {
        return new GradeCollection($student->grades);
    }
}
