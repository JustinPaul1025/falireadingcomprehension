<?php

namespace App\Http\Controllers;

use App\Models\Quiz;
use App\Models\Section;
use Illuminate\Http\Request;
use App\Models\ReadingMaterial;
use App\Http\Resources\Quiz\QuizCollection;
use App\Http\Resources\Sections\Section as sectionResource;
use App\Http\Resources\ReadingMaterial\ReadingMaterial as ReadingMaterialResource;

class QuizController extends Controller
{
    public function list(Request $request)
    {
        $quizzes = Quiz::query();

        if($request->filled('search')) {
            $quizzes = $quizzes->where('question', 'LIKE', '%'.$request->input('search').'%');
        }

        $quizzes = $quizzes->where('reading_material_id', $request->input('id'));
        
        $quizzes = $quizzes->paginate(10);

        return new QuizCollection($quizzes);
    }

    public function index(ReadingMaterial $readingMaterial)
    {
        $section = Section::where('id', $readingMaterial->section_id)->first();

        return view('sections.quiz', ['material' => new ReadingMaterialResource($readingMaterial), 'section' => new SectionResource($section)]);
    }

    public function store(Request $request)
    {
        $material = ReadingMaterial::findOrFail($request->input('id'));

        $quiz = $material->quizzes()->create([
            'question' => $request->input('question'),
            'answer' => $request->input('answer'),
            'choices' => $request->input('choices')
        ]);

        if($request->hasFile('quiz_img')) {
            $quiz->addMedia($request->file('quiz_img'))->toMediaCollection('quiz_img');
        }

        return response()->json($material);
    }

    public function update(Request $request)
    {   
        $quiz = Quiz::findOrFail($request->input('id'));

        if($request->hasFile('quiz_img')) {
            $media = collect($quiz->media)->firstWhere('collection_name', 'quiz_img');

            if($media != null) {
                $media->delete();
            }

            $quiz->addMedia($request->file('quiz_img'))->toMediaCollection('quiz_img');
        }

        return response()->json($quiz->update([
            'question' => $request->input('question'),
            'answer' => $request->input('answer'),
            'choices' => $request->input('choices')
        ]));
    }

    public function destroy(Quiz $quiz)
    {
        return response()->json($quiz->delete());
    }
}
