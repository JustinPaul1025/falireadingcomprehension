<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Events\StudentReading;
use App\Http\Resources\Sections\SectionCollection;

class SectionController extends Controller
{
    public function all()
    {
        return response()->json(Section::all());
    }

    public function list(Request $request)
    {
        $sections = Section::query();

        if($request->filled('search')) {
            $sections = $sections->where('name', 'LIKE', '%'.$request->input('search').'%');
        }
        if(!auth()->user()->isAdmin()) {
            $sections = $sections->where('user_id', auth()->id());
        }
        
        $sections = $sections->paginate(10);

        return new SectionCollection($sections);
    }

    public function archivedList(Request $request)
    {
        $sections = Section::query(); 

        $sections = $sections->onlyTrashed();

        $sections = $sections->orderByRaw('name ASC');
        $sections = $sections->paginate(10);
        return new SectionCollection($sections);
    }

    public function availableList()
    {
        return response()->json(Section::where('user_id', null)->get());
    }

    public function store(Request $request)
    {
        broadcast(new StudentReading('yeah', 'Reading'));

        if($request->input('teacher_id') > 0) {
            $teacher = $request->input('teacher_id');
        } else {
            $teacher = null;
        }
        Section::create([
            'name' => $request->input('name'),
            'user_id' => $teacher 
        ]);

        return response()->json('Section Created');
    }

    public function update(Section $section, Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|unique:sections,name,' . $section->id
        ]);

        if($request->input('teacher_id') > 0) {
            $teacher = $request->input('teacher_id');
        } else {
            $teacher = null;
        }

        $section->update([
            'name' => $request->input('name'),
            'user_id' => $teacher 
        ]);

        return response()->json($section);
    }

    public function destroy(Section $section)
    {
        $students = $section->students;
        foreach($students as $student) {
            $student->delete();
        } 
        return response()->json($section->delete());
    }

    public function restore($id)
    {
        $section = Section::withTrashed()->find($id)->restore();
        $students = Student::where('section_id', $id)->onlyTrashed()->get();
        foreach($students as $student) {
            $student->restore();
        } 
        return response()->json($section);
    }
}
