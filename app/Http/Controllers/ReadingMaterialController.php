<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Models\Student;
use Twilio\Rest\Client;
use Spatie\PdfToText\Pdf;
use Illuminate\Http\Request;
use App\Models\ReadingMaterial;
use App\Http\Resources\Sections\Section as SectionResource;
use App\Http\Resources\ReadingMaterial\ReadingMaterialCollection;
use App\Http\Resources\ReadingMaterial\ReadingMaterial as ReadingMaterialResource;

class ReadingMaterialController extends Controller
{
    public function index(Section $section)
    {
        return view('sections.reading-materials', ['section' => new SectionResource($section)]);
    }

    public function list(Request $request)
    {
        $reading = ReadingMaterial::query();

        if($request->filled('search')) {
            $reading = $reading->where('title', 'LIKE', '%'.$request->input('search').'%');
        }
        
        $reading = $reading->where('section_id', $request->input('id'));
        
        $reading = $reading->paginate(10);

        return new ReadingMaterialCollection($reading);
    }

    public function show(ReadingMaterial $readingMaterial)
    {
        return response()->json(new ReadingMaterialResource($readingMaterial));
    }

    public function store(Request $request)
    {
        beginTransaction();
        try {
            $readingMaterial = ReadingMaterial::create([
                'title' => $request->input('title'),
                'section_id' => $request->input('id'),
                'words' => $request->input('words'),
                'img_src' => $request->input('img_src')
            ]);

            if($request->hasFile('story_img')) {
                $readingMaterial->addMedia($request->file('story_img'))->toMediaCollection('story_img');
            }

            $students = Student::Where('section_id', $request->input('id'))->get();
            // $message = "Good Day! \n\nYou can now check our newly uploaded reading material in FALI Reading Comprehension app";
            // foreach ($students as $student) {
            //     $mobile_no = '+63';

            //     if($request->input('mother_contact_no') != '') {
            //         $mobile_no = $mobile_no.$student->mother_contact_no;
            //     } else {
            //         $mobile_no = $mobile_no.$student->father_contact_no;
            //     }

            //     $account_sid = getenv("TWILIO_SID");
            //     $auth_token = getenv("TWILIO_AUTH_TOKEN");
            //     $twilio_number = getenv("TWILIO_NUMBER");
            //     $client = new Client($account_sid, $auth_token);
            //     $client->messages->create($mobile_no, ['from' => $twilio_number, 'body' => $message]);
            // }

            commit();
        } catch (Exception $e) {
            rollback();
            throw $e;
        }

        return response()->json($readingMaterial);
    }

    public function update(Request $request)
    {
        $material = ReadingMaterial::findOrFail($request->input('id'));

        beginTransaction();
        try {
            $material->update([
                'title' => $request->input('title'),
                'words' => $request->input('words'),
                'img_src' => $request->input('img_src')
            ]);

            if($request->file('story_img')) {
                $media = collect($material->media)->firstWhere('collection_name', 'story_img');
                
                if($media != null) {
                    $media->delete();
                }
                
                $material->addMedia($request->file('story_img'))->toMediaCollection('story_img');
            }

            commit();
        } catch (Exception $e) {
            rollback();
            throw $e;
        }

        return response()->json($material);
    }

    public function destroy(ReadingMaterial $readingMaterial)
    {
        $terms = $readingMaterial->terms;
        foreach($terms as $term) {
            $media = collect($term->media)->firstWhere('collection_name', 'term_img');
            if($media != null) {
                $media->delete();
            }
        }

        $sentences = $readingMaterial->sentences;
        foreach($sentences as $sentence) {
            $media = collect($sentence->media)->firstWhere('collection_name', 'sentence_img');
            if($media != null) {
                $media->delete();
            }
        }

        $media = collect($readingMaterial->media)->firstWhere('collection_name', 'story_img');
        if($media != null) {
            $media->delete();
        }

        return response()->json($readingMaterial->delete());
    }
}
