<?php

namespace App\Http\Controllers\Api;

use App\Models\Grade;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GradeController extends Controller
{
    public function index(Student $student)
    {
        return response()->json($student->grades);
    }

    public function grade(Request $request)
    {
        $grade = Grade::where('student_id', $request->input('studentID'))
        ->where('reading_material_id', $request->input('storyID'))
        ->first();
        return response()->json($grade);
    }
}
