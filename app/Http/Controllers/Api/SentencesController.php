<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\ReadingMaterial;
use App\Http\Controllers\Controller;
use App\Http\Resources\Sentence\SentenceCollection;

class SentencesController extends Controller
{
    public function index(ReadingMaterial $readingMaterial)
    {
        $sentence = $readingMaterial->sentences;

        return new SentenceCollection($sentence);
    }
}
