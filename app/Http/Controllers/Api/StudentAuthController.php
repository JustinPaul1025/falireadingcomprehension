<?php

namespace App\Http\Controllers\Api;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;

class StudentAuthController extends Controller
{
    public function login(Request $request)
    {
        $student = Student::with('section')->where('username', $request->input('username'))->where('password', $request->Input('password'))->first();

        if($student != null) {
            activity('Student Logged in Successfully')
                ->by($student)
                ->withProperties([
                    'id' => $student->id,
                    'first_name' => $student->first_name,
                    'last_name' => $student->last_name,
                ])
                ->log('Login Successful');
            return response()->json($student);
        } else {
            return response()->json(false);
        }
    }
}
