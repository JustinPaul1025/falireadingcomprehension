<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use function GuzzleHttp\json_decode;

class SpeechAceController extends Controller
{
    public function calculate(Request $request)
    {
        // $user = User::where('id', 1)->first();

        // $user->addMedia($request->file('audio'))->toMediaCollection('audio');
        // return $user->audioUrl();

        // $key="MBiZONjrB2VSVJxKXZzy5k6Erhp%2FphaocXyuZyUOnE49Kh8aYiY6hUmrTdw0IUMKL3zTvM5rGuKMr1M68MJWHJqaTuUWWryAzd7xr9nMuK%2BhWeSz%2B0mr6ALUdr5Xh9Fz​";
        // $ch = curl_init("https://api.speechace.co/api/scoring/text/v0.5/json?user_id=1234&dialect=en-us&key=".$key);
        // $opts = array(
        //     CURLOPT_POST => 1,
        //     CURLOPT_HTTPHEADER => array("Content-Type:multipart/form-data"),
        //     CURLOPT_POSTFIELDS => array(
        //         "text" => $request->input('sentence'),
        //         "user_audio_file" => curl_file_create($user->audioUrl()),
        //     ),
        //     CURLOPT_RETURNTRANSFER => true
        // );

        // curl_setopt_array($ch,$opts);
        // $raw = curl_exec($ch);
        // if(curl_errno($ch) > 0) {
        //     echo("There was an error using the speechace api: ".curl_error($ch));
        // }

        // curl_close($ch);
        
        // $raw = json_decode($raw, true);

        // $media = collect($user->media)->firstWhere('collection_name', 'audio');
        // if($media != null) {
        //     $media->delete();
        // }

        // return response()->json($raw);


        $sentence = rtrim($request->input('sentence'), ". ");
        $read = strtoLower($request->input('read'));
        $sentence = strtoLower($sentence);
        $explodeSentence = explode(" ", $sentence);
        $explodeRead = explode(" ", $read);
        $errors = 0;
        for ($ctr = 0; $ctr <= count($explodeSentence)-1; $ctr++ ) {
            if(empty($explodeRead[$ctr])) {
                $errors = $errors+1;
            } else {
                if(soundex($explodeSentence[$ctr]) != soundex($explodeRead[$ctr])) {
                    $errors = $errors+1;
                }
            }
        }

        return response()->json($errors);
    }

    public function checkMissing(Request $request)
    {
        $sentence = rtrim($request->input('sentence'), ". ");
        $read = strtoLower($request->input('read'));
        $sentence = strtoLower($sentence);
        $explodeSentence = explode(" ", $sentence);
        $explodeRead = explode(" ", $read);
        $sentence = '';
        for ($ctr = 0; $ctr <= count($explodeSentence)-1; $ctr++ ) {
            if(empty($explodeRead[$ctr])) {
                if($ctr == 0){
                    $sentence = "<b style='color: red'>".$explodeSentence[$ctr].'</b>';
                } else {
                    $sentence = $sentence." <b style='color: red'>".$explodeSentence[$ctr].'</b>';
                }
            } else {
                if(soundex($explodeSentence[$ctr]) != soundex($explodeRead[$ctr])) {
                    if($ctr == 0){
                        $sentence = "<b style='color: red'>".$explodeSentence[$ctr].'</b>';
                    } else {
                        $sentence = $sentence." <b style='color: red'>".$explodeSentence[$ctr].'</b>';
                    }
                } else {
                    $check = $this->doubleCheck($explodeSentence[$ctr], $explodeRead[$ctr]);
                    if($check) {
                        if($ctr == 0){
                            $sentence = $explodeSentence[$ctr];
                        } else {
                            $sentence = $sentence.' '.$explodeSentence[$ctr];
                        }
                    } else {
                        if($ctr == 0){
                            $sentence = "<b style='color: red'>".$explodeSentence[$ctr].'</b>';
                        } else {
                            $sentence = $sentence." <b style='color: red'>".$explodeSentence[$ctr].'</b>';
                        }
                    }
                }
            }
        }
        return response()->json(strtoUpper($sentence));
    }

    private function doubleCheck(string $sentence, string $read)
    {
        if(($sentence[0] == 'p' && $read[0] == 'f') || ($sentence[0] == 'f' && $read[0] == 'p')) {
            return false;
        }
        for($i=0; $i<strlen($sentence);$i++) {
            if ($sentence[$i] == "o" || $sentence[$i] == "u") {
                if(strlen($read)-1 < $i) {
                    if($read[$i-1] == 'i' || $read[$i-1] == 'e' || $read[$i-1] == 'a') {
                        return false;
                    }
                } else {
                    if($read[$i] == 'i' || $read[$i] == 'e' || $read[$i] == 'a') {
                        return false;
                    }
                }
            }
            if($sentence[$i] == "i" || $sentence[$i] == "e") {
                if(strlen($read)-1 < $i) {
                    if($read[$i-1] == 'o' || $read[$i-1] == 'u' || $read[$i-1] == 'a') {
                        return false;
                    }
                } else {
                    if($read[$i] == 'o' || $read[$i] == 'u' || $read[$i] == 'a') {
                        return false;
                    }
                }
            }
            if($sentence[$i] == "a") {
                if(strlen($read)-1 < $i) {
                    if($read[$i-1] == 'o' || $read[$i-1] == 'u' || $read[$i-1] == 'i' || $read[$i-1] == 'e') {
                        return false;
                    }
                } else {
                    if($read[$i] == 'o' || $read[$i] == 'u' || $read[$i] == 'i' || $read[$i] == 'e') {
                        return false;
                    }
                } 
            }
        }
        return true;
    }
}
