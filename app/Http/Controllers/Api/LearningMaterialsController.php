<?php

namespace App\Http\Controllers\Api;

use App\Models\Simulation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Simulation\SimulationCollection;

class LearningMaterialsController extends Controller
{
    public function index()
    {   
        $simulations = Simulation::get();

        return new SimulationCollection($simulations);
    }
}
