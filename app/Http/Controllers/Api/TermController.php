<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\ReadingMaterial;
use App\Http\Controllers\Controller;
use App\Http\Resources\Term\TermCollection;

class TermController extends Controller
{
    public function index(ReadingMaterial $readingMaterial)
    {
        $term = $readingMaterial->terms;

        return new TermCollection($term);
    }
}
