<?php

namespace App\Http\Controllers\Api;

use App\Models\Grade;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Models\ReadingMaterial;
use App\Http\Controllers\Controller;
use App\Http\Resources\Quiz\QuizCollection;

class QuizController extends Controller
{
    public function index(ReadingMaterial $readingMaterial)
    {
        return new QuizCollection($readingMaterial->quizzes);
    }

    public function saveResult(Request $request) {
        $matchThese = ['reading_material_id' => $request->input('material_id'), 'student_id' => $request->input('student_id')];
        $grade = Grade::where($matchThese);
        if($grade->first()) {
            if($grade->first()->comprehension == null) {
                $grade->update([
                    'comprehension' => $request->input('result')
                ]);
            } 

            Notification::create([
                'reading_material_id' => $request->input('material_id'),
                'student_id' => $request->input('student_id'),
                'event' => 'Taking a quiz',
                'is_seen' => false
            ]);
            return response()->json($grade);
        } else {
            $grade = Grade::create([
                'reading_material_id' => $request->input('material_id'),
                'student_id' => $request->input('student_id'),
                'comprehension' => $request->input('result')
            ]);
            return response()->json($grade);
        }
        
    }
}
