<?php

namespace App\Http\Controllers\Api;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function changePassword(Student $student, Request $request)
    {
        return $student->update([
            'password' => $request->input('password')
        ]);
    }
}
