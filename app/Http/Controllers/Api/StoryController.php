<?php

namespace App\Http\Controllers\Api;

use App\Models\Grade;
use App\Models\Section;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Events\StudentReading;
use App\Models\ReadingMaterial;
use App\Http\Controllers\Controller;
use App\Http\Resources\ReadingMaterial\ReadingMaterialCollection;
use App\Http\Resources\ReadingMaterial\ReadingMaterial as ReadingMaterialResource;
use App\Models\Notification;

class StoryController extends Controller
{
    public function index(Section $section)
    {
        return new ReadingMaterialCollection($section->readingMaterials);
    }

    public function show(ReadingMaterial $readingMaterial)
    {
        $readingMaterial = new ReadingMaterialResource($readingMaterial);
        return response()->json($readingMaterial);
    }

    public function recordGrade(Request $request)
    {
        $readingMaterial = ReadingMaterial::where('id', $request->input('material_id'))->first();
        $totalWords = $readingMaterial->words;
        $score = $totalWords - $request->input('errors');
        $result = ($score/$totalWords)*100;
        $matchThese = ['reading_material_id' => $request->input('material_id'), 'student_id' => $request->input('student_id')];
        $grade = Grade::where($matchThese);
        $student = Student::where('id', $request->input('student_id'))->first();

        event(new StudentReading($student, 'Reading'));
        if($grade->first()) {
            if($grade->reading != null) {
                $grade->update([
                    'reading' => $result,
                    'student_reading' => $request->input('studentReading')
                ]); 
            }
            return response()->json($grade);
        } else {
            $grade = Grade::create([
                'reading_material_id' => $request->input('material_id'),
                'student_id' => $request->input('student_id'),
                'reading' => $result,
                'student_reading' => $request->input('studentReading')
            ]);
            
            Notification::create([
                'reading_material_id' => $request->input('material_id'),
                'student_id' => $request->input('student_id'),
                'event' => 'Reading Story',
                'is_seen' => false
            ]); 
            return response()->json($grade);
        }
    }
}
