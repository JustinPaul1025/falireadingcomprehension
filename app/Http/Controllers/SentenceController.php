<?php

namespace App\Http\Controllers;

use App\Models\Sentence;
use Illuminate\Http\Request;
use App\Models\ReadingMaterial;
use App\Http\Resources\Sentence\SentenceCollection;

class SentenceController extends Controller
{
    public function index(ReadingMaterial $readingMaterial)
    {
        $sentence = $readingMaterial->sentences;

        return new SentenceCollection($sentence);
    }

    public function update(Request $request)
    {
        $sentence = Sentence::findOrFail($request->input('id'));

        $request->validate([
            'sentence' => 'required',
        ]);

        $sentence->update([
            'sentence' => $request->input('sentence')
        ]);

        if($request->hasFile('sentence_img')) {
            $media = collect($sentence->media)->firstWhere('collection_name', 'sentence_img');
            if($media != null) {
                $media->delete();
            }

            $sentence->addMedia($request->file('sentence_img'))->toMediaCollection('sentence_img');
        }

        return response()->json('done');
    }

    public function store(Request $request)
    {
        $request->validate([
            'sentence' => 'required|max:255',
        ]);

        $sentence = Sentence::create([
            'reading_material_id' => $request->input('id'),
            'sentence' => $request->input('sentence'),
        ]);

        if($request->hasFile('sentence_img')) {
            $sentence->addMedia($request->file('sentence_img'))->toMediaCollection('sentence_img');
        }

        return response()->json('done');
    }

    public function destroy(Sentence $sentence)
    {
        $media = collect($sentence->media)->firstWhere('collection_name', 'sentence_img');
        if($media != null) {
            $media->delete();
        }

        return response()->json($sentence->delete());
    }
}
