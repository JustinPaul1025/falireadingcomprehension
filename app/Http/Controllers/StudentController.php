<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Models\Student;
use Twilio\Rest\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Exports\StudentExport;
use App\Imports\ImportStudents;
use Maatwebsite\Excel\Facades\Excel;
use CreatvStudio\Itexmo\Facades\Itexmo;
use App\Http\Resources\Student\StudentCollection;
use App\Http\Resources\Sections\Section as SectionResource;

class StudentController extends Controller
{
    public function studentList(Request $request)
    {
        $students = Student::query(); 

        if($request->filled('search')) {
            if($request->filled('section_id')) {
                $search = $request->input('search');
                $students = $students->where('section_id', $request->input('section_id'))
                ->where(function($q) use ($search){
                    $q->where('first_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('last_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('username', 'LIKE', '%'.$search.'%')
                    ->orWhere('middle_name', 'LIKE', '%'.$search.'%');
                });
            } else {
                $students = $students->where('first_name', 'LIKE', '%'.$request->input('search').'%')
                ->orWhere('last_name', 'LIKE', '%'.$request->input('search').'%')
                ->orWhere('username', 'LIKE', '%'.$request->input('search').'%')
                ->orWhere('middle_name', 'LIKE', '%'.$request->input('search').'%');
            }
        } else {
            if($request->filled('section_id')) {
                $students = $students->where('section_id', $request->input('section_id'));
            }
        }

        $students = $students->paginate(20);
        return new StudentCollection($students);
    }

    public function archivedList(Request $request)
    {
        $students = Student::query(); 

        $students = $students->where('section_id', $request->input('id'))->onlyTrashed();

        $students = $students->orderByRaw('last_name ASC, first_name ASC');
        $students = $students->paginate(10);
        return new StudentCollection($students);
    }

    public function list(Request $request)
    {
        $students = Student::query(); 

        if($request->filled('search')) {
            $students = Student::where('first_name', 'LIKE', '%'.$request->input('search').'%')
            ->orWhere('last_name', 'LIKE', '%'.$request->input('search').'%')
            ->orWhere('username', 'LIKE', '%'.$request->input('search').'%')
            ->orWhere('middle_name', 'LIKE', '%'.$request->input('search').'%');
        }

        $students = $students->where('section_id', $request->input('id'));

        $students = $students->orderByRaw('last_name ASC, first_name ASC');
        $students = $students->paginate(20);
        
        return new StudentCollection($students);
    }

    public function index(Section $section)
    {
        return view('students.index', ['section' => new SectionResource($section)]);
    }

    public function store(Request $request)
    {
        $count = Student::withTrashed()->count();
        $password = $request->input('first_name').($count+1);
        $username = $request->input('first_name').'_'.$request->input('last_name');

        $message = "Username: ".$username."\nPassword: ".$password;
        $address = $request->input('prk').', '.$request->input('prk').', '.$request->input('city');

        $student = Student::create([
            'first_name' => strtolower($request->input('first_name')),
            'middle_name' => strtolower($request->input('middle_name')),
            'last_name' => strtolower($request->input('last_name')),
            'middle_name' => $request->input('middle_name'),
            'birth_Date' => $request->input('birthdate'),
            'address' => $address,
            'gender' => $request->input('gender'),
            'mother_name' => $request->input('mother_name'),
            'mother_contact_no' => $request->input('mother_contact_no'),
            'father_name' => $request->input('father_name'),
            'father_contact_no' => $request->input('father_contact_no'),
            'section_id' => $request->input('section_id'),
            'username' => $username,
            'password' => $password,
        ]);

        $mobile_no = '0';

        if($request->input('mother_contact_no') != '') {
            $mobile_no = $mobile_no.$request->input('mother_contact_no');
        } else {
            $mobile_no = $mobile_no.$request->input('father_contact_no');
        }

        // Itexmo::to($mobile_no)->content($message)->send();

        // $account_sid = getenv("TWILIO_SID");
        // $auth_token = getenv("TWILIO_AUTH_TOKEN");
        // $twilio_number = getenv("TWILIO_NUMBER");
        // $client = new Client($account_sid, $auth_token);
        // $client->messages->create($mobile_no, ['from' => $twilio_number, 'body' => $message]);

        return response()->json('done');
    }

    public function update(Student $student, Request $request)
    {
        return $student->update([
            'first_name' => strtolower($request->input('first_name')),
            'middle_name' => strtolower($request->input('middle_name')),
            'last_name' => strtolower($request->input('last_name')),
            'username' => $request->input('username')
        ]);
    }

    public function destroy(Student $student)
    {
        return $student->delete();
    }

    public function restore($id)
    {
        return Student::withTrashed()->find($id)->restore();
    }

    public function uploadFile(Request $request)
    {
        Excel::import(new ImportStudents, $request->file('file'));
        return 'imported!';
    }

    public function export(Section $section) 
    {
        return (new StudentExport($section->id))->download($section->name.'.xlsx');
    }
}
