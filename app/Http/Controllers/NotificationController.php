<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Notification\NotificationCollection;

class NotificationController extends Controller
{
    public function list()
    {
        return new NotificationCollection(Notification::get());
    }

    public function read()
    {
        $notifications = DB::table('notifications')->update(['is_seen' => true]);

        return $notifications;
    }
}
