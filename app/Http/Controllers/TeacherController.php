<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function list()
    {
        return response()->json(User::role('Teacher')->get());
    }
}
