<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Models\Section;
use App\Types\RoleType;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\Notifications\UserVerification;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\User as UserResource;

class UserController extends Controller
{
    public function list(Request $request)
    {
        $users = User::query();
        if ($request->filled('sortBy')) {
            $users = User::role($request->input('sortBy'));
        }
        if($request->filled('search')) {
            $users = User::where('first_name', 'LIKE', '%'.$request->input('search').'%')
            ->orWhere('last_name', 'LIKE', '%'.$request->input('search').'%')
            ->orWhere('username', 'LIKE', '%'.$request->input('search').'%')
            ->orWhere('email', 'LIKE', '%'.$request->input('search').'%');
        }
        $users = $users->paginate(10);
        return new UserCollection($users);
    }
    public function store(UserRequest $request)
    {
        $address = $request->input('prk').', '.$request->input('prk').', '.$request->input('city');
        
        beginTransaction();
        try {
            $user = User::create([
                'first_name' => strtolower($request->input('first_name')),
                'middle_name' => strtolower($request->input('middle_name')),
                'last_name' => strtolower($request->input('last_name')),
                'username' => $request->input('username'),
                'email' => $request->input('email'),
                'address' => $address,
                'birth_Date' => $request->input('birth_date'),
                'gender' => $request->input('gender'),
                'password' => Hash::make($request->input('password')),
                'is_verified' => true
            ]);

            if($request->input('registerAs') == 'Administrator') {
                $user->assignRole(RoleType::ADMINISTRATOR);
            } else if ($request->input('registerAs') == 'Teacher') {
                $user->assignRole(RoleType::TEACHER);
            } else {
                $user->assignRole(RoleType::STUDENT);
            }

            if($request->input('section_id') > 0) {
                $section = Section::find($request->input(['section_id']));
                $section->update([
                    'user_id' => $user->id
                ]);
            }

            // $user->notify(new UserVerification($user));
                
            commit();
        } catch (Exception $e) {
            rollback();
            throw $e;
        }

        return response()->json('Saved');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json('user deleted');
    }

    public function update(User $user, UserRequest $request)
    {
        $user->update([
            'first_name' => strtolower($request->input('first_name')),
            'middle_name' => strtolower($request->input('middle_name')),
            'last_name' => strtolower($request->input('last_name')),
            'username' => $request->input('username'),
            'address' => $request->input('address'),
            'birth_Date' => $request->input('birth_date'),
            'gender' => $request->input('gender'),
            'email' => $request->input('email'),
        ]);

        $user->removeRole($user->roles->first());

        if($request->input('registerAs') == 'Administrator') {
            $user->assignRole(RoleType::ADMINISTRATOR);
        } else if ($request->input('registerAs') == 'Teacher') {
            $user->assignRole(RoleType::TEACHER);
        } else {
            $user->assignRole(RoleType::STUDENT);
        }

        return response()->json($user);
    }

    public function verifyUser(User $user)
    {
        if($user->is_verified) {
            return redirect()->route('login');
        }

        $user->update([
            'is_verified' => true
        ]);

        return redirect()->route('login')->with('jsAlert', 'Your Account Has Been Verified!');
    }

    public function checkEmail($email)
    {
        if (User::where('email', '=', $email)->exists()) {
            return response()->json(true);
        } else {
            return response()->json(false); 
        }
    }

    public function checkUsername($username)
    {
        if (User::where('username', '=', $username)->exists()) {
            return response()->json(true);
        } else {
            return response()->json(false); 
        }
    }

    public function detail()
    {
        return new UserResource(auth()->user());
    }

    public function changePassword(User $user, Request $request)
    {
        $user->update([
            'password' => Hash::make($request->input('password'))
        ]);

        return $user;
    }
}
