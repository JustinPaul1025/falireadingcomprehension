<?php

namespace App\Http\Resources\Quiz;

use Illuminate\Http\Resources\Json\JsonResource;

class Quiz extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $img = collect($this->media)->firstWhere('collection_name', 'quiz_img');

        return [
            'id' => $this->id,
            'question' => $this->question,
            'choices' => json_decode($this->choices),
            'answer' => $this->answer,
            'img_url' => optional($img)->getUrl()
        ];
    }
}
