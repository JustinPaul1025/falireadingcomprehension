<?php

namespace App\Http\Resources\Term;

use Illuminate\Http\Resources\Json\JsonResource;

class Term extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $img = collect($this->media)->firstWhere('collection_name', 'term_img');

        return [
            'id' => $this->id,
            'word' => $this->word,
            'definition' => $this->definition,
            'file_url' => optional($img)->getUrl()
        ];
    }
}
