<?php

namespace App\Http\Resources\Grade;

use Illuminate\Http\Resources\Json\JsonResource;

class Grade extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reading' => $this->reading,
            'comprehension' => $this->comprehension,
            'reading_material' => $this->readingMaterial,
            'student_reading' => $this->student_reading,
            'comment' => $this->comment($this->reading, $this->comprehension)
        ];
    }

    private function comment($reading, $comprehension) {
        if($comprehension != null || $reading != null) {
            return 'Complete The Activity First';
        }

        $result = ($comprehension + $reading)/2;

        if($result < 20.1) {
            return 'Good';
        }
        if($result > 20 && $result < 40.01) {
            return 'Very Good';
        }
        if($result > 40 && $result < 60.01) {
            return 'Excellent';
        }
        if($result > 60 && $result < 80.01) {
            return 'Great';
        }
        if($result == 100) {
            return 'Incredibly Good';
        }
    }
}
