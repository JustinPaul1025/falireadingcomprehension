<?php

namespace App\Http\Resources\Sentence;

use Illuminate\Http\Resources\Json\JsonResource;

class Sentence extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $img = collect($this->media)->firstWhere('collection_name', 'sentence_img');

        return [
            'id' => $this->id,
            'sentence' => $this->sentence,
            'file_url' => optional($img)->getUrl()
        ];
    }
}
