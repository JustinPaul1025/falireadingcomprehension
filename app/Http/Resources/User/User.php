<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $address = explode(",", $this->address);

        return [
            'id' => $this->id,
            'name' => ucwords(strtolower($this->first_name)).' '.ucwords(strtolower($this->middle_name[0])).'. '.ucwords(strtolower($this->last_name)),
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'username' => $this->username,
            'gender' => $this->gender,
            'prk' => count($address) == 1 ? $address[0] : '',
            'brgy' => count($address) == 2 ? $address[1] : '',
            'city' => count($address) == 3 ? $address[2] : '',
            'birth_date' => $this->birth_date,
            'role' => $this->getRoleNames()->first()
        ];
    }
}
