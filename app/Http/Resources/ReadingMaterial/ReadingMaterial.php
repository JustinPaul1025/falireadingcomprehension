<?php

namespace App\Http\Resources\ReadingMaterial;

use Illuminate\Http\Resources\Json\JsonResource;

class ReadingMaterial extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $img = collect($this->media)->firstWhere('collection_name', 'story_img');

        return [
            'section' => $this->section,
            'id' => $this->id,
            'words' => $this->words,
            'title' => $this->title,
            'readingmaterials' => 'none',
            'story' => $this->story,
            'prepared_by' => $this->section->user,
            'img_url' => $this->img_src
        ];
    }
}
