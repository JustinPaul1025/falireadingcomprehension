<?php

namespace App\Http\Resources\Student;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Mockery\Undefined;

class Student extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $address = explode(", ", $this->address);

        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'username' => $this->username,
            'password' => $this->password,
            'prk' => count($address) == 1 ? $address[0] : '',
            'brgy' => count($address) == 2 ? $address[1] : '',
            'city' => count($address) == 3 ? $address[2] : '',
            'birth_date' => $this->birth_date,
            'father_name' => $this->father_name,
            'mother_name' => $this->mother_name,
            'gender' => $this->gender,
            'mother_contact_no' => $this->mother_contact_no,
            'father_contact_no' => $this->father_contact_no,
            'section' => $this->section,
            'year' => Carbon::parse($this->created_at)->year
        ];
    }
}
