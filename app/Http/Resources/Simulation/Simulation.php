<?php

namespace App\Http\Resources\Simulation;

use Illuminate\Http\Resources\Json\JsonResource;

class Simulation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $video = collect($this->media)->firstWhere('collection_name', 'video');
        $thumb = collect($this->media)->firstWhere('collection_name', 'thumbnail');

        return [
            'id' => $this->id,
            'title' => $this->title,
            'file_type' => $this->file_type,
            'uploader' => $this->uploader,
            'video' => optional($video)->getUrl(),
            'thumbnail' => optional($thumb)->getUrl(),
        ];
    }
}
