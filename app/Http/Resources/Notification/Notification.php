<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Resources\Json\JsonResource;

class Notification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'student' => $this->student,
            'reading_material' => $this->readingMaterial,
            'event' => $this->event,
            'is_seen' => $this->is_seen,
            'created_at' => $this->created_at
        ];
    }
}
