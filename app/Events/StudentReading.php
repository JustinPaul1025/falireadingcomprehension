<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StudentReading implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $student;

    public $type;

    public function __construct($student, $type)
    {
        $this->student = $student;
        $this->type  = $type;
    }


    public function broadcastOn()
    {
        return new PresenceChannel('student-reading');
    }

    public function broadcastAs()
    {
        return 'studentReading';
    }
}
