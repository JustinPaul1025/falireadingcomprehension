<?php

namespace App\Exports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class StudentExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(int $section)
    {
        $this->section = $section;
    }

    public function query()
    {
        return Student::query()->orderByRaw('last_name ASC, first_name ASC')->where('section_id', $this->section);
    }

    public function headings(): array
    {
        return [
            'Last Name',
            'First Name',
            'Middle Name',
            'Username',
            'Password',
        ];
    }

    public function map($student): array
    {
        return [
            $student->last_name,
            $student->first_name,
            $student->middle_name,
            $student->username,
            $student->password,
        ];
    }
}
