<?php

namespace App;

use Illuminate\Support\Facades\URL;

class App 
{
    public static function get()
    {
        return [
            'is_admin' => auth()->check() ? auth()->user()->isAdmin() : false,
            'is_teacher' => auth()->check() ? auth()->user()->isTeacher() : false,
            'is_student' => auth()->check() ? auth()->user()->isStudent() : false,
            'base_route' => URL::to('/')
        ];
    }
}