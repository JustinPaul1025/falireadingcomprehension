<?php

namespace App\Imports;

use App\User;
use App\Models\Section;
use App\Models\Student;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportStudents implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $getID = explode("/", URL::current());

        if(empty($row[1])){
            return;
        } else {
            $count = Student::withTrashed()->count();
            $username = $row[1].'_'.$row[3];
        }
        for($check = true;$check;){
            if (User::where('username', '=', $username)->exists()) {
                $username = $username.rand(100, 999);
            } else {
                $check = false;
            }
        }
        if(strtolower($row[0]) == 'section' && strtolower($row[5]) == 'password') {
            return;
        } else {
            // $message = "Good Day! \n\nHere's the account of ".$row[1]." for Fali Reading Comprehension App \nUsername: ".$row[4]."\nPassword: ".$row[5];

            // $mobile_no = '+63';

            // if($row[7] != '' || $row[7] != null) {
            //     $mobile_no = $mobile_no.$row[7];
            // } else {
            //     $mobile_no = $mobile_no.$row[9];
            // }

            // $account_sid = getenv("TWILIO_SID");
            // $auth_token = getenv("TWILIO_AUTH_TOKEN");
            // $twilio_number = getenv("TWILIO_NUMBER");
            // $client = new Client($account_sid, $auth_token);
            // $client->messages->create($mobile_no, ['from' => $twilio_number, 'body' => $message]);
            $student = new Student([
                'section_id' => $getID[count($getID) - 1],
                'first_name' => $row[1],
                'middle_name' => $row[2],
                'last_name' => $row[3],
                'username' => $username,
                'password' => $row[1].($count+1),
                'mother_name' => $row[8],
                'mother_contact_no' => $row[9],
                'father_name' => $row[6],
                'father_contact_no' => $row[7],
            ]);

            return $student;
        }

    }
}
