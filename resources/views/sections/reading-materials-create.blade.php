@extends('layouts.app')

@section('content')
    <create-reading-material :section="{{ json_encode(Request::segment(3)) }}"></create-reading-material>
@endsection