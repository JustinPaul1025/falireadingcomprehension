@extends('layouts.app')

@section('content')
    <edit-reading-material :material="{{ json_encode(Request::segment(3)) }}"></edit-reading-material>
@endsection