@extends('layouts.app')

@section('content')
    <quiz-materials :material="{{ json_encode($material) }}" :section="{{ json_encode($section) }}"></quiz-materials>
@endsection