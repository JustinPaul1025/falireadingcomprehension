@extends('layouts.app')

@section('content')
    <reading-materials :section="{{ json_encode($section) }}"></reading-materials>
@endsection