@extends('layouts.app')

@section('content')
    <student-lists :section="{{ json_encode($section) }}"></student-lists>
@endsection