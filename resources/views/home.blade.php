@extends('layouts.app')

@section('content')
<div class="leading-normal tracking-normal text-orange-500 gradient">
    <div>
        <div class="px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">
            <!--Left Col-->
            {{-- <div class="flex flex-col w-full md:w-2/5 justify-center items-start text-center md:text-left">
                <p class="uppercase tracking-loose w-full">FALI READING COMPREHENSION APP</p>
                <h1 class="my-4 text-5xl font-bold leading-tight">Sustaining Education Amidst The Pandemic and Crisis</h1>
            </div> --}}
            <div class="w-full px-56 pt-8 text-center">
                <img class="w-full z-50" src="{{ url('images/login_img.jpg') }}">
            </div>
            
        </div>
    </div>
</div>
@endsection
