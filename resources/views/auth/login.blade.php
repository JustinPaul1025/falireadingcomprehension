<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Fali | Reading Comprehension - Login</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @if(session()->has('jsAlert'))
        <script type="text/javascript">
            window.onload = function () { 
            alert("Your Account has been verified");
            //dom not only ready, but everything is loaded
            }
        </script>
    @endif 
</head>
<body>
    <div>
        <div class="lg:flex">
            <div class="lg:w-1/2 xl:max-w-screen-sm">
                <div class="py-12 bg-indigo-100 lg:bg-white flex justify-center lg:justify-start lg:px-12">
                    <div class="cursor-pointer flex items-center">
                        <div>
                            <img class="object-fill h-40 w-40" src="{{ url('images/logo.png') }}" alt="">
                        </div>
                        <div class="text-4xl fali-color tracking-wide ml-2 font-semibold">Reading Comprehension</div>
                    </div>
                </div>
                <div class="mt-10 px-12 sm:px-24 md:px-48 lg:px-12 lg:mt-16 xl:px-24 xl:max-w-2xl">
                    <h2 class="text-center text-4xl fali-color font-display font-semibold lg:text-left xl:text-5xl
                    xl:text-bold">Log in</h2>
                    <div class="mt-12">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div>
                                <div class="text-sm font-bold text-gray-700 tracking-wide">Username</div>
                                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-orange-500 @error('username') border-red-500 @enderror" type="text" id="username" placeholder="enter your username" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                                @error('username')
                                    <span class="invalid-feedback text-orange-500" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-8">
                                <div class="flex justify-between items-center">
                                    <div class="text-sm font-bold text-gray-700 tracking-wide">
                                        Password
                                    </div>
                                </div>
                                <input class="w-full text-lg py-2 border-b focus:outline-none focus:border-orange-500 @error('password') border-red-500 @enderror" type="password" placeholder="Enter your password" id="password" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mt-10">
                                <button type="submit" class="fali-bg text-gray-100 p-4 w-full rounded-full tracking-wide
                                font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-orange-500 shadow-lg">
                                    Log In
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="hidden lg:flex items-center justify-center flex-1 h-screen">
                <div class="max-w-full transform duration-200 cursor-pointer">
                    <img class="object-fill h-full w-full" src="{{ url('images/login_img.png') }}" alt="">
                </div>  
            </div>
        </div>
    </div>
</body>
</html>