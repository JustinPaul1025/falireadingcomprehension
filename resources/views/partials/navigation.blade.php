<header class="bg-orange-500 py-6">
    <div class="container mx-auto flex justify-between items-center px-6">
        <div>
            <a href="{{ url('/home') }}" class="text-lg font-semibold text-gray-100 no-underline mr-5">
                {{ 'Reading Comprehension' }}
            </a>
        </div>
        @if(auth()->user() && auth()->user()->is_verified)
        <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
            <div class="text-sm lg:flex-grow">
                <a href="{{ route('sections') }}" class="{{ Request::segment(1) == 'sections' || Request::segment(1) == 'students' || Request::segment(1) == 'materials' ? 'underline' : '' }} block mt-4 mr-4 lg:inline-block lg:mt-0 text-white hover:text-white hover:underline">
                    Sections
                </a>
                @if (auth()->user()->isAdmin())
                    <a href="{{ route('users') }}" class="{{ Request::segment(1) == 'users' ? 'underline' : '' }} block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white hover:underline mr-4">
                        User Accounts
                    </a>
                    <a href="{{ route('reports') }}" class="{{ Request::segment(1) == 'reports' ? 'underline' : '' }} block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white hover:underline mr-4">
                        Student Reports
                    </a>
                    <a href="{{ route('logs') }}" class="{{ Request::segment(1) == 'logs' ? 'underline' : '' }} block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white hover:underline mr-4">
                        User Logs
                    </a>
                @endif
                @if (auth()->user()->isTeacher())
                    <a href="{{ route('simulations') }}" class="{{ Request::segment(1) == 'simulations' ? 'underline' : '' }} block mt-4 mr-4 lg:inline-block lg:mt-0 text-white hover:text-white hover:underline">
                        Video Tutorials
                    </a>
                @endif
                <a href="{{ route('change-password') }}" class="{{ Request::segment(1) == 'change-password' ? 'underline' : '' }} block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white hover:underline mr-4">
                    Change Password
                </a>
                {{-- <a href="{{ route('reports') }}" class="{{ Request::segment(1) == 'reports' ? 'underline' : '' }} block mt-4 lg:inline-block lg:mt-0 text-white hover:text-white hover:underline mr-4">
                    Utilities
                </a> --}}
            </div>
            <Notify></Notify>
        </div>
        @endif
        <nav class="space-x-4 text-gray-300 text-sm sm:text-base">
            @guest
                <a class="no-underline hover:underline" href="{{ route('login') }}">{{ __('Login') }}</a>
                @if (Route::has('register'))
                    <a class="no-underline hover:underline" href="{{ route('register') }}">{{ __('Register') }}</a>
                @endif
            @else
                <span>{{ Auth::user()->name }}</span>

                <a href="{{ route('logout') }}"
                   class="no-underline hover:underline"
                   onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            @endguest
        </nav>
    </div>
</header>