/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import VModal from 'vue-js-modal/dist/index.nocss.js'
import 'vue-js-modal/dist/styles.css'
import store from './store/vuex'
import VuejsPaginate from 'vuejs-paginate'
import Swal from 'sweetalert2'
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import { global } from './mixins';

window.Vue = require('vue');
window.Mammoth = require('mammoth/lib/index.js');
window.Swal = Swal;

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('user-index', require('./components/users/index.vue').default);
Vue.component('class-section', require('./components/sections/index.vue').default);
Vue.component('video-simulations', require('./components/simulations/index.vue').default);
Vue.component('reading-materials', require('./components/sections/readingMaterials.vue').default);
Vue.component('quiz-materials', require('./components/sections/quiz.vue').default);
Vue.component('student-lists', require('./components/students/index.vue').default);
Vue.component('report-index', require('./components/report/index.vue').default);
Vue.component('create-reading-material', require('./components/sections/createReadingMaterial.vue').default);
Vue.component('edit-reading-material', require('./components/sections/editReadingMaterial.vue').default);
Vue.component('test', require('./components/Test.vue').default);
Vue.component('notify', require('./components/Notify.vue').default);
Vue.component('logs', require('./components/Logs.vue').default);
Vue.component('change-password', require('./components/ChangePassword.vue').default);
Vue.component('paginate', VuejsPaginate);
Vue.component('loading', Loading);

Vue.use(VModal, { dynamicDefault: { draggable: true } })

Vue.mixin(global)

var filter = function(text, length, clamp){
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
};

Vue.filter('truncate', filter);

const app = new Vue({
    el: '#app',
    store,
});
