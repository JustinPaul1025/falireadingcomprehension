export default {
    namespaced: true,
    state: {
        students: {},
        errors: [],
        pagination: {
            meta: {
                last_page: 1
            }
        }
    },
    getters: {
        students: state => state.students,
        pagination: state => state.pagination,
        errors: state => state.errors
    },
    mutations: {
        setStudents (state, payload) {
            state.students = payload
        },
        setErrors (state, payload) {
            state.errors = payload
        },
        setPagination (state, payload) {
            if (payload == {}) {
                state.pagination = {
                    meta: {
                        last_page: 1
                    }
                }
            } else {
                state.pagination = payload
            }
        },
    },
    actions: {
        async addStudent ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/student/${payload.id}`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async updateStudent ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.put(`/student-update/${payload.id}`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    commit('setErrors', error.response.data.errors)
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async getStudents ({ commit }, payload = {}) {
            await axios.get(`/getStudents?page=` + payload.params.page, {
                params: payload.params
            })
            .then(response => {
                commit('setStudents', response.data)
                commit('setPagination', response.data)
            })
        },
        async getStudentList ({ commit }, payload = {}) {
            await axios.get(`/getStudentList?page=` + payload.params.page, {
                params: payload.params
            })
            .then(response => {
                commit('setStudents', response.data)
                commit('setPagination', response.data)
            })
        },
        async archivedStudents ({ commit }, payload = {}) {
            await axios.get(`/getArchivedStudents?page=` + payload.params.page, {
                params: payload.params
            })
            .then(response => {
                commit('setStudents', response.data)
                commit('setPagination', response.data)
            })
        },
        async deleteStudent ({ commit }, payload = {}) {
            try {
                const response = await axios.delete(`/${payload.id}/student`)
                return response
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async restoreStudent ({ commit }, payload = {}) {
            try {
                const response = await axios.get(`/${payload.id}/student-restore`)
                return response
            } catch (error) {
                return Promise.reject(error);
            }
        },
    }
}