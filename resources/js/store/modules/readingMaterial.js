export default {
    namespaced: true,
    state: {
        materials: {},
        errors: [],
        pagination: {
            meta: {
                last_page: 1
            }
        }
    },
    getters: {
        materials: state => state.materials,
        pagination: state => state.pagination,
        errors: state => state.errors
    },
    mutations: {
        setMaterials (state, payload) {
            state.materials = payload
        },
        setErrors (state, payload) {
            state.errors = payload
        },
        setPagination (state, payload) {
            if (payload == {}) {
                state.pagination = {
                    meta: {
                        last_page: 1
                    }
                }
            } else {
                state.pagination = payload
            }
        },
    },
    actions: {
        async addReadingMaterial ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/material`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async updateReadingMaterial ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/reading-material-update`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    commit('setErrors', error.response.data.errors)
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async getMaterials ({ commit }, payload = {}) {
            await axios.get(`/getMaterials?page=` + payload.params.page, {
                params: payload.params
            })
            .then(response => {
                commit('setMaterials', response.data)
                commit('setPagination', response.data)
            })
        },
        async deleteMaterial ({ commit }, payload = {}) {
            try {
                const response = await axios.delete(`/${payload.id}/material`)
                return response
            } catch (error) {
                return Promise.reject(error);
            }
        },
    }
}