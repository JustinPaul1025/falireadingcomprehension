export default {
    namespaced: true,
    state: {
        quizzes: {},
        errors: [],
        pagination: {
            meta: {
                last_page: 1
            }
        }
    },
    getters: {
        quizzes: state => state.quizzes,
        errors: state => state.errors,
        pagination: state => state.pagination
    },
    mutations: {
        setQuizzes (state, payload) {
            state.quizzes = payload
        },
        setErrors (state, payload) {
            state.errors = payload
        },
        setPagination (state, payload) {
            if (payload == {}) {
                state.pagination = {
                    meta: {
                        last_page: 1
                    }
                }
            } else {
                state.pagination = payload
            }
        },
    },
    actions: {
        async saveQuiz ({ dispatch, commit }, payload = {}) {
            try {
                console.log(payload);
                const response = await axios.post(`/quiz`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    commit('setErrors', error.response.data.errors)
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async updateQuiz ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/quiz-update`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    commit('setErrors', error.response.data.errors)
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async getQuizzes ({ commit }, payload = {}) {
            await axios.get(`/getQuizzes?page=` + payload.params.page, {
                params: payload.params
            })
            .then(response => {
                commit('setQuizzes', response.data)
                commit('setPagination', response.data)
            })
        },
        async deleteQuiz ({ commit }, payload = {}) {
            try {
                const response = await axios.delete(`/${payload.id}/quiz`)
                return response
            } catch (error) {
                return Promise.reject(error);
            }
        },
    }
}