export default {
    namespaced: true,
    state: {
        terms: {}
    },
    getters: {
        terms: state => state.terms
    },
    mutations: {
        setTerms (state, payload) {
            state.terms = payload
        },
    },
    actions: {
        async saveTerm ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/term`, payload)
                return response
            } catch (error) {
                if (error.response.status == 422){
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async updateTerm ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/term-update`, payload)
                return response
            } catch (error) {
                if (error.response.status == 422){
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async deleteTerm ({ commit }, payload = {}) {
            try {
                const response = await axios.delete(`/${payload.id}/term`)
                return response
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async getTerms ({ commit }, payload = {}) {
            console.log(payload.id)
            await axios.get(`/getTerms/${payload.id}`)
            .then(response => {
                commit('setTerms', response.data)
            })
        },
    }
}