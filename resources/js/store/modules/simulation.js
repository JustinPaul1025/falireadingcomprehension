export default {
    namespaced: true,
    state: {
        simulations: {},
        errors: [],
        pagination: {
            meta: {
                last_page: 1
            }
        }
    },
    getters: {
        simulations: state => state.simulations,
        pagination: state => state.pagination,
        errors: state => state.errors
    },
    mutations: {
        setSimulations (state, payload) {
            state.simulations = payload
        },
        setErrors (state, payload) {
            state.errors = payload
        },
        setPagination (state, payload) {
            if (payload == {}) {
                state.pagination = {
                    meta: {
                        last_page: 1
                    }
                }
            } else {
                state.pagination = payload
            }
        },
    },
    actions: {
        async saveSimulation ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/simulation`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async updateSimulation ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/simulation-update`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    commit('setErrors', error.response.data.errors)
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async getSimulations ({ commit }, payload = {}) {
            await axios.get(`/getSimulations?page=` + payload.params.page, {
                params: payload.params
            })
            .then(response => {
                commit('setSimulations', response.data)
                commit('setPagination', response.data)
            })
        },
        async deleteSimulation ({ commit }, payload = {}) {
            try {
                const response = await axios.delete(`/${payload.id}/simulation`)
                return response
            } catch (error) {
                return Promise.reject(error);
            }
        },
    }
}