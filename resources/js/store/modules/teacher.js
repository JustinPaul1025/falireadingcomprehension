export default {
    namespaced: true,
    state: {
        teachers: {},
    },
    getters: {
        teachers: state => state.teachers
    },
    mutations: {
        setTeachers (state, payload) {
            state.teachers = payload
        },
    },
    actions: {
        async getTeachers ({ commit }) {
            await axios.get(`/teachers`)
            .then(response => {
                commit('setTeachers', response.data)
            })
        },
    }
}