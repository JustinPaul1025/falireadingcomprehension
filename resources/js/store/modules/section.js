export default {
    namespaced: true,
    state: {
        sections: {},
        errors: [],
        pagination: {
            meta: {
                last_page: 1
            }
        }
    },
    getters: {
        sections: state => state.sections,
        errors: state => state.errors,
        pagination: state => state.pagination
    },
    mutations: {
        setSections (state, payload) {
            state.sections = payload
        },
        setErrors (state, payload) {
            state.errors = payload
        },
        setPagination (state, payload) {
            if (payload == {}) {
                state.pagination = {
                    meta: {
                        last_page: 1
                    }
                }
            } else {
                state.pagination = payload
            }
        },
    },
    actions: {
        async saveSection ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/section`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    commit('setErrors', error.response.data.errors)
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async updateSection ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.put(`/section/${payload.id}`, payload)
                commit('setErrors', {})
                return response
            } catch (error) {
                if (error.response.status == 422){
                    commit('setErrors', error.response.data.errors)
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async getSections ({ commit }, payload = {}) {
            await axios.get(`/getSections?page=` + payload.params.page, {
                params: payload.params
            })
            .then(response => {
                commit('setSections', response.data)
                commit('setPagination', response.data)
            })
        },
        async archivedSections ({ commit }, payload = {}) {
            await axios.get(`/getArchivedSections?page=` + payload.params.page, {
                params: payload.params
            })
            .then(response => {
                commit('setSections', response.data)
                commit('setPagination', response.data)
            })
        },
        async deleteSection ({ commit }, payload = {}) {
            try {
                const response = await axios.delete(`/${payload.id}/section`)
                return response
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async restoreSection ({ commit }, payload = {}) {
            try {
                const response = await axios.get(`/${payload.id}/section-restore`)
                return response
            } catch (error) {
                return Promise.reject(error);
            }
        },
    }
}