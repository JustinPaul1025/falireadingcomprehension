export default {
    namespaced: true,
    state: {
        sentences: {}
    },
    getters: {
        sentences: state => state.sentences
    },
    mutations: {
        setSentences (state, payload) {
            state.sentences = payload
        },
    },
    actions: {
        async saveSentence ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/sentence`, payload)
                return response
            } catch (error) {
                if (error.response.status == 422){
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async updateSentence ({ dispatch, commit }, payload = {}) {
            try {
                const response = await axios.post(`/sentence-update`, payload)
                return response
            } catch (error) {
                if (error.response.status == 422){
                    return error.response.data.errors
                } else {
                    return Promise.reject(error);
                }
            }
        },
        async deleteSentence ({ commit }, payload = {}) {
            try {
                const response = await axios.delete(`/${payload.id}/sentence`)
                return response
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async getSentences ({ commit }, payload = {}) {
            console.log(payload.id)
            await axios.get(`/getSentences/${payload.id}`)
            .then(response => {
                commit('setSentences', response.data)
            })
        },
    }
}