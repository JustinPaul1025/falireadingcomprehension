import Vue from 'vue'
import Vuex from 'vuex'

import user from './modules/user'
import section from './modules/section'
import teacher from './modules/teacher'
import simulation from './modules/simulation'
import readingMaterial from './modules/readingMaterial'
import quiz from './modules/quiz'
import student from './modules/student'
import term from './modules/term'
import sentence from './modules/sentence'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
    },

    modules: {
       user,
       section,
       teacher,
       simulation,
       readingMaterial,
       quiz,
       student,
       term,
       sentence
    },

    getters: {
    },

    mutations: {
    }
})

export default store