<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\QuizController;
use App\Http\Controllers\Api\TermController;
use App\Http\Controllers\Api\GradeController;
use App\Http\Controllers\Api\StoryController;
use App\Http\Controllers\Api\StudentController;
use App\Http\Controllers\Api\SentencesController;
use App\Http\Controllers\Api\SpeechAceController;
use App\Http\Controllers\Api\StudentAuthController;
use App\Http\Controllers\Api\LearningMaterialsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/student-login', [StudentAuthController::class, 'login']);

Route::get('/learning-materials', [LearningMaterialsController::class, 'index']);
Route::get('/stories/{section}', [StoryController::class, 'index']);
Route::get('/story/{readingMaterial}', [StoryController::class, 'show']);
Route::post('/story/submit-result', [StoryController::class, 'recordGrade']);

Route::get('/terms/{readingMaterial}', [TermController::class, 'index']);

Route::get('/quiz/{readingMaterial}', [QuizController::class, 'index']);
Route::post('/quiz/result-save', [QuizController::class, 'saveResult']);

Route::get('/sentence/{readingMaterial}', [SentencesController::class, 'index']);

Route::get('/grades/{student}', [GradeController::class, 'index']);
Route::post('/grade', [GradeController::class, 'grade']);

Route::put('/change-password/{student}', [StudentController::class, 'changePassword']);

Route::post('/story/calculate', [SpeechAceController::class, 'calculate']);
Route::post('/story/check-missing', [SpeechAceController::class, 'checkMissing']);
