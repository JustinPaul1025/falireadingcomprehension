<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LogController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\QuizController;
use App\Http\Controllers\TermController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\SentenceController;
use App\Http\Controllers\SimulationController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ReadingMaterialController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(auth()->user()) {
        return view('users.index');
    } else {
        return view('auth.login');
    }
});

Route::get('/verify-user/{user}', [UserController::class, 'verifyUser']);

Auth::routes();

Route::middleware('auth')->group(function () {

    Route::get('/home', [HomeController::class, 'index']);
    
    Route::view('/users', 'users.index')->name('users');
    Route::post('/user', [UserController::class, 'store']);
    Route::get('/getUsers', [UserController::class, 'list']);
    Route::delete('/{user}/user', [UserController::class, 'destroy']);
    Route::put('/user/{user}', [UserController::class, 'update']);
    Route::get('user-detail', [UserController::class, 'detail']);

    Route::view('/sections', 'sections.index')->name('sections');
    Route::post('/section', [SectionController::class, 'store']);
    Route::get('/getSections', [SectionController::class, 'list']);
    Route::get('/getArchivedSections', [SectionController::class, 'archivedList']);
    Route::get('/getAvailableSections', [SectionController::class, 'availableList']);
    Route::get('/getAllSections', [SectionController::class, 'all']);
    Route::delete('/{section}/section', [SectionController::class, 'destroy']);
    Route::put('/section/{section}', [SectionController::class, 'update']);
    Route::get('/{id}/section-restore', [SectionController::class, 'restore']);

    Route::view('/simulations', 'simulations.index')->name('simulations');
    Route::post('/simulation', [SimulationController::class, 'store']);
    Route::get('/getSimulations', [SimulationController::class, 'list']);
    Route::delete('/{simulation}/simulation', [SimulationController::class, 'destroy']);
    Route::post('/simulation-update', [SimulationController::class, 'update']);

    Route::get('/materials/{section}', [ReadingMaterialController::class, 'index']);
    Route::post('/material', [ReadingMaterialController::class, 'store']);
    Route::get('/material/{readingMaterial}', [ReadingMaterialController::class, 'show']);
    Route::get('/getMaterials', [ReadingMaterialController::class, 'list']);
    Route::post('/reading-material-update', [ReadingMaterialController::class, 'update']);
    Route::delete('/{readingMaterial}/material', [ReadingMaterialController::class, 'destroy']);
    Route::view('/reading-materials/create/{id}', 'sections.reading-materials-create');
    Route::view('/reading-materials/edit/{id}', 'sections.reading-materials-edit');

    Route::get('/list-quiz/{readingMaterial}', [QuizController::class, 'index']);
    Route::post('/quiz', [QuizController::class, 'store']);
    Route::get('/getQuizzes', [QuizController::class, 'list']);
    Route::post('/quiz-update', [QuizController::class, 'update']);
    Route::delete('/{quiz}/quiz', [QuizController::class, 'destroy']);

    Route::post('/term', [TermController::class, 'store']);
    Route::post('/term-update', [TermController::class, 'update']);
    Route::get('/getTerms/{readingMaterial}', [TermController::class, 'index']);
    Route::delete('/{term}/term', [TermController::class, 'destroy']);

    Route::post('/sentence', [SentenceController::class, 'store']);
    Route::post('/sentence-update', [SentenceController::class, 'update']);
    Route::get('/getSentences/{readingMaterial}', [SentenceController::class, 'index']);
    Route::delete('/{sentence}/sentence', [SentenceController::class, 'destroy']);
    
    Route::get('/students/{section}', [StudentController::class, 'index']);
    Route::post('/student/{section}', [StudentController::class, 'store']);
    Route::get('/getStudents', [StudentController::class, 'list']);
    Route::get('/getStudentList', [StudentController::class, 'studentList']);
    Route::get('/getArchivedStudents', [StudentController::class, 'archivedList']);
    Route::put('/student-update/{student}', [StudentController::class, 'update']);
    Route::delete('/{student}/student', [StudentController::class, 'destroy']);
    Route::get('/{id}/student-restore', [StudentController::class, 'restore']);
    Route::post('/student-upload/{id}', [StudentController::class, 'uploadFile']);
    Route::get('/student-export/{section}', [StudentController::class, 'export']);

    Route::view('/reports', 'report')->name('reports');
    Route::view('/logs', 'logs')->name('logs');

    Route::get('/user-logs', [LogController::class, 'list']);

    Route::get('/teachers', [TeacherController::class, 'list']);

    Route::get('/grades/{student}', [GradeController::class, 'studentGrade']);

    Route::get('/check-email/{email}', [UserController::class, 'checkEmail']);
    Route::get('/check-username/{username}', [UserController::class, 'checkUsername']);
    Route::get('/check-notification', [NotificationController::class, 'list']);
    Route::get('/read-notification', [NotificationController::class, 'read']);

    Route::view('/change-password', 'change-password')->name('change-password');
    Route::put('change-password/{user}', [UserController::class, 'changePassword']);
});

Route::view('/test', 'test');