<?php

use App\Models\GradingScale;
use Illuminate\Database\Seeder;

class GradingScalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GradingScale::create([
            'level' => 'INDEPENDENT',
            'reading_score' => '97-100',
            'comprehension_score' =>  '80-100'
        ]);

        GradingScale::create([
            'level' => 'INSTRUCTIONAL',
            'reading_score' => '90-96',
            'comprehension_score' =>  '59-79'
        ]);

        GradingScale::create([
            'level' => 'FRUSTRTED',
            'reading_score' => '0-89',
            'comprehension_score' =>  '0-58'
        ]);
    }
}
