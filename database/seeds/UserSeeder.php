<?php

use App\User;
use App\Types\RoleType;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'Admin',
            'middle_name' => 'Admin',
            'last_name' => 'Admin',
            'username' => 'Admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('123123'),
            'is_verified' => true
        ]);

        $user->assignRole(RoleType::ADMINISTRATOR);

        $user = User::create([
            'first_name' => 'Admin',
            'middle_name' => 'Admin',
            'last_name' => 'Admin',
            'username' => 'Teacher',
            'email' => 'teacher@mail.com',
            'password' => bcrypt('123123'),
            'is_verified' => true
        ]);

        $user->assignRole(RoleType::TEACHER);
    }
}
